var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list=function(req,res){
    res.render('bicicleta/index',{bicis:Bicicleta.allBicis});
}

exports.bicicleta_create_get=function(req,res){
    res.render('bicicleta/create');
}

exports.bicicleta_create_post=function(req,res){
    var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo,[req.body.latitud,req.body.longitud]);
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post=function(req,res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}

exports.bicicleta_edit=function(req,res){
    res.render('bicicleta/edit',{bici:Bicicleta.findById(req.body.id)});
}


exports.bicicleta_update=function(req,res){
    var bici= Bicicleta.findById(req.body.id)
    Bicicleta.removeById(req.body.id);
    bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo,[req.body.latitud,req.body.longitud]);
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}