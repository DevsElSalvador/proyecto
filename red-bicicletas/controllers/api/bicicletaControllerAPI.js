var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){   
    /*res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });*/
    Bicicleta.find({}, function(err, bicicletas){
        res.status(200).json({
            bicicletas: bicicletas
        });
    }); 
}

exports.bicicleta_create = function (req,res) {
    //console.dir(req.body);
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);   

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_update = function (req,res) {
    var bici = Bicicleta.findById(req.body.code);
    
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [ req.body.lat, req.body.lng];

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByID(req.body.code);
    res.status(204).send();    // 204: respuesta correcta sin contenido
}
