var Bicileta = require('../../models/bicicleta');
const Bicicleta = require('../../models/bicicleta');

//beforeEach(()=>{Bicicleta.allBicis=[];});

describe("Bicicleta.allBicis",()=>{
    it('comienza vacia',()=>{
        expect(Bicicleta.allBicis.length).toBe(3);
    })
});


describe("Bicicleta.add",()=>{
    it('agregando bici',()=>{
        //comprobando lista vacia
        expect(Bicicleta.allBicis.length).toBe(3);
        var a = new Bicicleta(4,'rojo','forza',[-34.6012424,-58.3861497]);
        Bicicleta.add(a);
        //comprobando que se añadiera la nueva bici
        expect(Bicicleta.allBicis.length).toBe(4);
        expect(Bicicleta.allBicis[3]).toBe(a);
    })
});


describe("Bicicleta.findById",()=>{
    it('comprobando que la ultima sea id 4',()=>{
        
        var a = new Bicicleta(5,'rojo','forza',[-34.6012424,-58.3861497]);
        Bicicleta.add(a);
        var bici = Bicicleta.findById(5);
        
        //comprobando que se añadiera la nueva bici
        expect(bici.id).toEqual(5);
    })
});

describe("Bicicleta.removeById",()=>{
    it('Eliminando la bici con id 5',()=>{
        //existencias 5 despues de agregar la de los test anteriores
        expect(Bicicleta.allBicis.length).toBe(5);                
        Bicicleta.removeById(5);
        
        expect(Bicicleta.allBicis.length).toBe(4);
    })
});