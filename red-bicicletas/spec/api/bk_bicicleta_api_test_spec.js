var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe("Bicicleta API",()=>{
    describe("Get bicicletas /",()=>{

        it('Status 200',()=>{
            expect(Bicicleta.allBicis.length).toBe(3);
          
            //comprobando que se añadiera la nueva bici
          //  expect(Bicicleta.allBicis.length).toBe(4);

            request.get("http://localhost:3000/api/bicicletas",function(error, response, body){
                expect(response.statusCode).toBe(200);
            });

        });

    });


    describe("Post bicicletas /create",()=>{

        it('Status 200',(done)=>{
            

            var headers={'content-type' : 'application/json'};
            var abici='{"id": 6,  "color": "magenta", "modelo": "forza","latitud": "-34.6116424", "longitud": "-58.3861497" }';

           request.post(
                    {
                        headers:headers,
                        url:'http://localhost:3000/api/bicicletas/create',
                        body:abici 
                    }, 
                    
                    function(error, response, body)
                    {
                        expect(response.statusCode).toBe(200);
                    }
                    
                );

            request.get("http://localhost:3000/api/bicicletas",function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(6).color).toBe("magenta");
                done();
            });

        });

    });


    describe("Delete bicicletas /delete",()=>{

        it('Status 200',(done)=>{
            

            var headers={'content-type' : 'application/json'};
            var idbici='{"id": 3}';

            request.delete(
                {
                    headers:headers,
                    url:'http://localhost:3000/api/bicicletas/delete',
                    body:idbici 
                }, 
                
                function(error, response, body)
                {
                    expect(response.statusCode).toBe(200);
                }
                
            );
                //ELIMINADA LA QUE SE AGREGO ANTERIORMENTE VUELVEN A QUEDAR 3 BICIS
            expect(Bicicleta.allBicis.length).toBe(3);
            done();
        });

    });



    
    

    describe("Edit bicicletas /edit",()=>{

        it('Status 200',(done)=>{
            

            var headers={'content-type' : 'application/json'};
            
            
                
            biciedit='{"id": 6,  "color": "rojizo", "modelo": "forza","latitud": "-34.6116424", "longitud": "-58.3861497" }';

           request.put(
                    {
                        headers:headers,
                        url:'http://localhost:3000/api/bicicletas/edit',
                        body:biciedit 
                    }, 
                    
                    function(error, response, body)
                    {
                        expect(response.statusCode).toBe(200);
                    }
                    
                );
                
            request.get("http://localhost:3000/api/bicicletas",function(error, response, body){
                expect(response.statusCode).toBe(200); 
                expect(Bicicleta.findById(6).color).toBe("rojizo"); 
                done();
            });

        });

    });



});